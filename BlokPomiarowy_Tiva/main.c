// Kontroler Bloków Diagnostycznych - TI TM4C123GH6PM
// Copyright (C) 2020 Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/* ----------------------- Includes -----------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include "tm4c123gh6pm.h"
#include "params.h"
#include "mbreg.h"

/* ----------------------- Function prototypes ------------------------------------*/

void GetADCReading(ULONG *ulADCReading);
void SysDelayMs(uint32_t ui32Ms);
void SysDelayUs(uint32_t ui32Us);
void GPIOPinUnlockGPIO(uint32_t ui32Port, uint8_t ui8Pins);

/* ----------------------- Static variables ------------------------------------*/

static ULONG  ulMotorTimerLoadValue = 0;
static BOOL   bMotorTimeout = FALSE;

int main(void)
{
    /*------------------------ Variables ------------------------------------------------*/

    BOOL bLastSensHigh = FALSE;
    BOOL bMotorActive = FALSE;
    ULONG ulADCReading = 0;
    UCHAR ucCommand = 0;
    UCHAR ucPA2 = 0;
    UCHAR ucPD7 = 0;

    /*------------------------ System Clock Configuration (50MHz) -----------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------ Enable floating point ------------------------------------*/

    FPULazyStackingEnable();
    FPUEnable();

    /*------------------------ GPIO Pin Configuration -----------------------------------*/

    //Enable GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    GPIOPinUnlockGPIO(GPIO_PORTD_BASE, GPIO_PIN_7); //unlock the PD7 GPIO

    // UART
    GPIOPinConfigure(GPIO_PC6_U3RX);
    GPIOPinConfigure(GPIO_PC7_U3TX);
    GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_6 | GPIO_PIN_7);

    //Motor control and ADC sensitivity (PA5)
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_3 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7, GPIO_PIN_5); //initial state

    //Motor status
    GPIOPinTypeGPIOInput(GPIO_PORTA_BASE, GPIO_PIN_2);
    GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_7);

    // External LED
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_5);

    #if (TIVA_ADC)

        //Tiva ADC configuration
        SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
        GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_0);
        ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);                            //configure sample sequencer (single-sample)
        ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH7 | ADC_CTL_IE | ADC_CTL_END);       //sample channel 7 (PD0) and generate interrupt
        ADCHardwareOversampleConfigure(ADC0_BASE, 64);                                           //64-sample hardware averaging
        ADCSequenceEnable(ADC0_BASE, 3);                                                         //enable sequencer

    #else

        //SSI configuration for LTC1298
        SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);
        GPIOPinConfigure(GPIO_PD0_SSI1CLK);
        GPIOPinConfigure(GPIO_PD1_SSI1FSS);
        GPIOPinConfigure(GPIO_PD2_SSI1RX);
        GPIOPinConfigure(GPIO_PD3_SSI1TX);
        GPIOPinTypeSSI(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
        SSIConfigSetExpClk(SSI1_BASE, SysCtlClockGet(), SSI_FRF_NMW, SSI_MODE_MASTER, 150000, 12);
        SSIEnable(SSI1_BASE);

    #endif

    /*------------------------------ Motor Error Timer --------------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER1A);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    ulMotorTimerLoadValue = SysCtlClockGet() * MOTOR_TIMEOUT;

    /*------------------------------ MODBUS Initialization ----------------------------------------*/

    (void) eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);
    (void) eMBEnable();

    /*------------------- MODBUS Coils/Discrete Inputs Initialization -----------------------------*/

    ucPA2 = (GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_2) >> 2) & 1;
    ucPD7 = (GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_7) >> 7) & 1;

    //Motor status
    if (!ucPA2) xMBUtilSetBits(ucMBDiscretes, 0, 1, 0); //Discrete #1 = 0 (Wsunięty)
    else if (!ucPD7) xMBUtilSetBits(ucMBDiscretes, 0, 1, 1); //Discrete #1 = 1 (Wysunięty)

    //Input pin status
    if (ucPA2 == ucPD7) xMBUtilSetBits(ucMBDiscretes, 1, 1, 1); //Discrete #2 = Motor Error
    xMBUtilSetBits(ucMBDiscretes, 2, 1, ucPA2); //Discrete #3 = PA2
    xMBUtilSetBits(ucMBDiscretes, 3, 1, ucPD7); //Discrete #4 = PD7

    while (1)
    {
        /*--------------------------------- MODBUS ----------------------------------------------------*/

        (void) eMBPoll();

        /*--------------------------------- Motor Control ---------------------------------------------*/

        //New command received
        if (bMBCoilIsWritten(REG_COILS_START) && !bMotorActive)
        {
            ucCommand = xMBUtilGetBits(ucMBCoils, 0, 1); //Coil #1 (0 = Wsuń, 1 = Wysuń)

            //IF input pin status differs from command
            if ((!ucCommand && ucPA2) || (ucCommand && ucPD7))
            {
                //Release brake and wait 2ms
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3, GPIO_PIN_3);
                SysDelayMs(2);

                //Active motor
                if (!ucCommand) GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6 | GPIO_PIN_7, GPIO_PIN_6);
                else GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6 | GPIO_PIN_7, GPIO_PIN_7);
                bMotorActive = TRUE;

                xMBUtilSetBits(ucMBDiscretes, 4, 1, 1); //Discrete #5 = 1 (Motor Active)

                //Start motor error timer
                TimerDisable(TIMER1_BASE, TIMER_BOTH);
                bMotorTimeout = FALSE;
                TimerLoadSet(TIMER1_BASE, TIMER_BOTH, ulMotorTimerLoadValue);
                TimerEnable(TIMER1_BASE, TIMER_BOTH);
            }
        }

        ucPA2 = (GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_2) >> 2) & 1;
        ucPD7 = (GPIOPinRead(GPIO_PORTD_BASE, GPIO_PIN_7) >> 7) & 1;

        //Input pin status and motor errors
        if (ucPA2 != ucPD7)
        {
            xMBUtilSetBits(ucMBDiscretes, 1, 1, 0); //clear motor error
            if (!ucPA2) xMBUtilSetBits(ucMBDiscretes, 0, 1, 0); //Discrete #1 = 0 (Wsunięty)
            else if (!ucPD7) xMBUtilSetBits(ucMBDiscretes, 0, 1, 1); //Discrete #1 = 1 (Wysunięty)
        }
        else if (!bMotorActive)
        {
            xMBUtilSetBits(ucMBDiscretes, 1, 1, 1); //motor error
        }

        xMBUtilSetBits(ucMBDiscretes, 2, 1, ucPA2);
        xMBUtilSetBits(ucMBDiscretes, 3, 1, ucPD7);

        //Motor command complete (or error timeout)
        if (bMotorActive && ((((ucCommand && !ucPD7) || (!ucCommand && !ucPA2)) && (ucPA2 != ucPD7)) || bMotorTimeout))
        {
            //Motor status
            if (!ucPA2) xMBUtilSetBits(ucMBDiscretes, 0, 1, 0); //Discrete #1 = 0 (Wsunięty)
            else if (!ucPD7) xMBUtilSetBits(ucMBDiscretes, 0, 1, 1); //Discrete #1 = 1 (Wysunięty)

            //Engage brake and de-activate motor
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3, 0);
            TimerDisable(TIMER1_BASE, TIMER_BOTH);
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6 | GPIO_PIN_7, 0);
            bMotorActive = FALSE;

            xMBUtilSetBits(ucMBDiscretes, 4, 1, 0); //Discrete #5 = 0 (Motor Inactive)

            //Update motor errors
            if (bMotorTimeout || (ucPA2 == ucPD7)) xMBUtilSetBits(ucMBDiscretes, 1, 1, 1);
            else xMBUtilSetBits(ucMBDiscretes, 1, 1, 0); //clear error
        }

        /*--------------------------------- ADC Sequencing --------------------------------------------*/

        //Read ADC when the measurement unit is active
        if (!bMotorActive && (ucPA2 != ucPD7) && !ucPD7)
        {
            //Check next reading at default sensitivity
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_5, GPIO_PIN_5);
            GetADCReading(&ulADCReading);

            if ((ulADCReading > 0xFF) && bLastSensHigh) //HIGH->NORMAL SENSITIVITY
            {
                bLastSensHigh = FALSE;
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_5, GPIO_PIN_5);
                GetADCReading(&ulADCReading); //repeat reading
                usMBInputReg[0] = (USHORT) ulADCReading; //store reading in Modbus input register
            }
            else if (ulADCReading < 0xE0 && !bLastSensHigh) //NORMAL->HIGH SENSITIVITY
            {
                bLastSensHigh = TRUE;
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_5, 0);
                GetADCReading(&ulADCReading); //repeat reading
                usMBInputReg[0] = (USHORT) ulADCReading; //store reading in Modbus input register
                usMBInputReg[0] |= (1 << 15); //store sensitivity status in MSB of register
            }
            else
            {
                if (bLastSensHigh) //repeat reading with latest (high) sensitivity
                {
                    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_5, 0);
                    GetADCReading(&ulADCReading);
                }

                usMBInputReg[0] = (USHORT) ulADCReading; //store reading in Modbus input register
                if (bLastSensHigh) usMBInputReg[0] |= (1 << 15);
            }
        }
    }
}

void GetADCReading(ULONG *ulADCReading)
{
    SysDelayUs(100);

    #if (TIVA_ADC)

        //Get ADC reading
        ADCIntClear(ADC0_BASE, 3);
        ADCProcessorTrigger(ADC0_BASE, 3);
        while(!ADCIntStatus(ADC0_BASE, 3, false)) {}
        ADCSequenceDataGet(ADC0_BASE, 3, ulADCReading);

    #else

        SSIDataPut(SSI1_BASE, 0b00001101); //send instruction
        while (SSIBusy(SSI1_BASE)) {}
        SSIDataGet(SSI1_BASE, ulADCReading); //read data
        *ulADCReading = (USHORT)(((((float)*ulADCReading * 5) / 3.3f)) + 0.5f); //scale to match Tiva ADC

    #endif
}

void Timer1IntHandler(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER1_BASE, true);
    TimerIntClear(TIMER1_BASE, ui32status);
    bMotorTimeout = TRUE;
}

void SysDelayMs(uint32_t ui32Ms)
{
    SysCtlDelay(ui32Ms * (SysCtlClockGet() / 3 / 1000));
}

void SysDelayUs(uint32_t ui32Us)
{
    SysCtlDelay(ui32Us * (SysCtlClockGet() / 3 / 1000000));
}

void GPIOPinUnlockGPIO(uint32_t ui32Port, uint8_t ui8Pins)
{
    HWREG(ui32Port + GPIO_O_LOCK) = GPIO_LOCK_KEY;      // Unlock the port
    HWREG(ui32Port + GPIO_O_CR) |= ui8Pins;             // Unlock the Pin
    HWREG(ui32Port + GPIO_O_LOCK) = 0;                  // Lock the port
}
